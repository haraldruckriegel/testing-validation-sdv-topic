# Testing and Validation

[Wiki documentation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/testing-validation-sdv-topic/-/wikis/home) 

# Related SDV projects:
-  [Eclipse Ibeji](https://projects.eclipse.org/projects/automotive.ibeji) 
-  [OpenADX](https://openmobility.eclipse.org/)
-  [OpenMobility](https://openmobility.eclipse.org/)
- Hybrid simulation based on Qemu / KVM or HVF / SystemC or FMI…

# Use cases:


# Cross Initiatives Collaboration:
- Linaro collaboration on the Qemu part
- OpenADX/OpenMobility
- Carla Simulator
- Autoware Foundation?
- AVCC?
- AWS/Graviton


# Notes: 

Testing and validation will be done at various levels of accuracy and independence.
There is no one size fits all because the cost of conducting a physically accurate car digital twin in a digital world that present realistic physics properties can be way too high in terms of time and money. So, while there can be millions of tests per day on “software only”, there may be tens of close to reality tests.
Emulated HW testing is the missing element that can be driven by Eclipse SDV.
Need to create more detailed/specific taxonomy, agree on value adding initial steps/projects/activities.






